package com.example.admin.mycalculator;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private RadioGroup RG;
    private RadioButton rbAdd, rbMinus, rbMultiple, rbDivide;
    private EditText firstNum, secondNum;
    private TextView solution;
    private Button bt;
    private int result=0 , num1= 0, num2=0;
    private long start, stop;
    private float time;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bt = (Button) findViewById(R.id.button);
        bt.setOnClickListener(MainActivity.this);

        firstNum = (EditText) findViewById(R.id.firstNumber);
        secondNum = (EditText) findViewById(R.id.secondNumber);
        solution = (TextView) findViewById(R.id.solution);
        rbAdd = (RadioButton) findViewById(R.id.rbAdd);
        rbMinus = (RadioButton) findViewById(R.id.rbMinus);
        rbMultiple = (RadioButton) findViewById(R.id.rbMultiple);
        rbDivide = (RadioButton) findViewById(R.id.rbDivide);

        RG = (RadioGroup) findViewById(R.id.radioGroup2);

        RG.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                acceptNumbers();
                calculate(i);

            }
        });

        showToast(getWeithAndHeight(MainActivity.this,""));
    }

    public static String getWeithAndHeight(Activity activity, String url) {
        DisplayMetrics dm = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels;
        int height = dm.heightPixels;
        String w=new String(""+width);
        String h=new String(""+height);
        url="width = " + w +" height = " +h;
        return url;
    }

    public void onClick(View view){
        acceptNumbers();
        if(view == bt){
            calculate(RG.getCheckedRadioButtonId());
        }
    }

    private void showToast(String msg) {
        Toast message = Toast.makeText(getBaseContext(), msg, Toast.LENGTH_SHORT);

        message.show();
    }

    private void acceptNumbers(){
        try{
            num1 = Integer.parseInt(firstNum.getText().toString());
        }
        catch (NumberFormatException ex){
            showToast("Please enter only number");
        }
        try {
            num2 = Integer.parseInt(secondNum.getText().toString());
        }
        catch (NumberFormatException ex){
            showToast("Please enter only number");
        }
    }

    private void calculate(int i){
        i = RG.getCheckedRadioButtonId();
        start = System.currentTimeMillis();
        switch(i){
            case R.id.rbAdd:
                result = num1 + num2;
                break;
            case R.id.rbMinus:
                result = num1 - num2;
                break;
            case R.id.rbMultiple:
                result = num1 * num2;
                break;
            case  R.id.rbDivide:
                try {
                    result = num1 / num2;
                }
                catch (ArithmeticException e){
                    showToast("Please divide by a non-zero number");
                }
                break;
        }
        stop = System.currentTimeMillis();
        time = stop - start;
        Log.d("Calculation: ","computation time ="+ time);
        solution.setText(" "+Integer.toString(result));
    }

    public void onSwitchClicked(View v){
        //Get reference of TextView from XML layout
        TextView tView = (TextView) findViewById(R.id.switchStatus);

        //Is the switch on?
        boolean on = ((Switch) v).isChecked();

        if(on)
        {
            //Do something when switch is on
            tView.setText("ON");
        }
        else
        {
            //Do something when switch is off
            tView.setText("OFF");
        }
    }
}
