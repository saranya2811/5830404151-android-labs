package th.ac.kku.mokkharat.saranya;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class SecondAcivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second_acivity);

        TextView txtView = (TextView)findViewById(R.id.solution);
        Bundle bundle = getIntent().getExtras();
        //String text = bundle.getString("Message");
        //String getConnect = bundle.getString("Connect");
        String getName = bundle.getString("Name");
        String getPhone = bundle.getString("Phone");
        String getString = bundle.getString("Str");

        txtView.setText(getName + " " +getString + " " + getPhone);
    }
}
