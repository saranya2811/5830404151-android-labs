package th.ac.kku.mokkharat.saranya;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button submitBut = (Button) findViewById(R.id.bt);

        //final String str = " has phone number as ";

        submitBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText name = (EditText) findViewById(R.id.name);
                EditText phone = (EditText) findViewById(R.id.phone);
                String str = getString(R.string.str);
                String name1 = name.getText().toString();
                String phone1 = phone.getText().toString();
                Intent intent = new Intent(MainActivity.this,SecondAcivity.class);
                intent.putExtra("Name",name1);
                intent.putExtra("Phone",phone1);
                intent.putExtra("Str",str);
                //intent.putExtra("Message",str);
                startActivity(intent);
            }
        });
    }
}
