package com.example.saranya.firebasetest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Saranya on 4/18/2018.
 */

public class User {

    public  String username;
    public  String email;

    public User(){

    }

    public  User(String username, String email) {
        this.username = username;
        this.email = email;
    }

    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("username", username);
        result.put("email", email);
        return  result;
    }
}
