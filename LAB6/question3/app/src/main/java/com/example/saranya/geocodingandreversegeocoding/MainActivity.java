package com.example.saranya.geocodingandreversegeocoding;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    EditText latitudeEdit, longitudeEdit, textAddress;
    TextView infoText;
    Button fetchButton;
    RadioGroup radioGroup;
    RadioButton rg_loc, rg_add;
    String string;
    int mode = 1;
    private String TAG;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        latitudeEdit = (EditText) findViewById(R.id.lat);
        longitudeEdit = (EditText) findViewById(R.id.lng);
        textAddress = (EditText) findViewById(R.id.address);

        radioGroup = findViewById(R.id.rg);

        rg_loc = findViewById(R.id.firstChoice);
        rg_add = findViewById(R.id.secondChoice);

        fetchButton = (Button) findViewById(R.id.bt);

        infoText = (TextView) findViewById(R.id.solution);

        textAddress.setEnabled(false);

        TAG = this.getLocalClassName();

        fetchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onButtonClicked(fetchButton);

            }
        });
        CheckRadio();
    }

    private void CheckRadio(){
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radiogroup, int checkedId) {
                if(rg_loc.isChecked()){
                    latitudeEdit.setEnabled(true);
                    longitudeEdit.setEnabled(true);
                    textAddress.setEnabled(false);
                    latitudeEdit.requestFocus();
                    mode = 1;
                }
                else {
                    latitudeEdit.setEnabled(false);
                    longitudeEdit.setEnabled(false);
                    textAddress.setEnabled(true);
                    textAddress.requestFocus();
                    mode = 2;
                }
            }
        });
    }

    public void onButtonClicked(View view) {
        Location loc = new Location("");

        try {
            loc.setLatitude(Double.parseDouble(latitudeEdit.getText().toString()));
            loc.setLongitude(Double.parseDouble(longitudeEdit.getText().toString()));
        }catch (Exception e){
            Log.e(TAG, "Impossible to connect to Geocoder", e);
        }
        string = textAddress.getText().toString();

        if (mode == 1){
            getAddressFromLocation(loc, this, new GeocoderHandler());
        } else {
            addressFromName(string,this,new GeocoderHandler());
        }
    }

    private final class GeocoderHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            String result;
            Bundle bundle;
            switch (message.what) {
                case 1:
                    bundle = message.getData();
                    result = bundle.getString("addressFromName");
                    break;
                case 2:
                    bundle = message.getData();
                    result = bundle.getString("addressFromLoc");
                    break;
                default:
                    result = null;
            }
            // replace by what you need to do
            infoText.setText(result);
        }
    }



    public void getAddressFromLocation(
            final Location location, final Context context, final Handler handler) {
        Thread thread = new Thread() {
            @Override
            public void run() {
                Geocoder geocoder = new Geocoder(context, Locale.getDefault());
                String result = null;
                try {
                    List<Address> list = geocoder.getFromLocation(
                            location.getLatitude(), location.getLongitude(), 1);
                    if (list != null && list.size() > 0) {
                        Address address = list.get(0);
                        result = address.getAddressLine(0) + ", "
                                + address.getLocality();
                    }
                } catch (IOException e) {
                    Log.e(TAG, "Impossible to connect to Geocoder", e);
                } finally {
                    Message msg = Message.obtain();
                    msg.setTarget(handler);
                    if (result != null) {
                        msg.what = 2;
                        Bundle bundle = new Bundle();
                        bundle.putString("addressFromLoc", result);
                        msg.setData(bundle);
                    } else
                        msg.what = 0;
                    msg.sendToTarget();
                }
            }
        };
        thread.start();
    }

    public void addressFromName(
            final String solution, final Context context, final Handler handler) {
        Thread thread = new Thread() {
            @Override
            public void run() {
                Geocoder geocoder = new Geocoder(context, Locale.getDefault());
                String result = null;
                try {
                    List<Address> list2 = geocoder.getFromLocationName(solution, 1);
                    if (list2 != null && list2.size() > 0) {
                        Address address = list2.get(0);
                        result = "Lat:" + address.getLatitude() + " Lng:" + address.getLongitude();
                    }
                } catch (IOException e) {
                    Log.e(TAG, "Impossible to connect to Geocoder", e);
                } finally {
                    Message msg = Message.obtain();
                    msg.setTarget(handler);
                    if (result != null) {
                        msg.what = 1;
                        Bundle bundle = new Bundle();
                        bundle.putString("addressFromName", result);
                        msg.setData(bundle);
                    } else
                        msg.what = 0;
                    msg.sendToTarget();
                }
            }
        };
        thread.start();
    }
}