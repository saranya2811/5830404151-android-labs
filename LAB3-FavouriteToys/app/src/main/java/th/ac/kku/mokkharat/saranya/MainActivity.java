package th.ac.kku.mokkharat.saranya;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_main);

        TextView txtGreen = (TextView)findViewById(R.id.green);
        txtGreen.setText("Green");
        txtGreen.setTextSize(30);
        txtGreen.setGravity(Gravity.CENTER);
    }
}
