package com.example.saranya.webapicaller;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    private Exception exception;
    private ProgressBar progressBar;
    private String urlVal;
    private EditText urlText;
    private Button bt;
    private TextView responseView;
    private String text;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progressBar = (ProgressBar)findViewById(R.id.progressBar);
        urlText = (EditText) findViewById(R.id.urlText);
        responseView = (TextView) findViewById(R.id.responseView);
        bt = (Button) findViewById(R.id.queryButton);

        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new RetrieveFeedTask().execute();
            }
        });
    }




class RetrieveFeedTask extends AsyncTask<Void, Void, String> {

        private  Exception exception;

        protected  void onPreExecute(){
            progressBar.setVisibility(View.VISIBLE);
            responseView.setText("");
        }

        @Override
        protected String doInBackground(Void... voids) {
            urlVal = urlText.getText().toString();
            try {
                URL url = new URL(urlVal);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                try {
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    StringBuilder stringBuilder = new StringBuilder();
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        stringBuilder.append(line).append("\n");
                    }
                    bufferedReader.close();
                    return stringBuilder.toString();
                }
                finally{
                    urlConnection.disconnect();
                }
            }
            catch(Exception e) {
                Log.e("ERROR", e.getMessage(), e);
                return null;
            }
        }

        protected void onPostExecute(String response) {
            if(response == null){
                response = "There was an error";
            }
            progressBar.setVisibility(View.GONE);
            Log.i("INFO","response = " + response);

            try {
                JSONObject object = (JSONObject) new JSONTokener(response).nextValue();
                text = object.getString("ip");
                responseView.setText(text);



            }catch (Exception e){
                e.printStackTrace();
            }

        }
    }

}

